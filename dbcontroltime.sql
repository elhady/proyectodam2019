-- MySQL dump 10.16  Distrib 10.1.40-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: ControlSysDb
-- ------------------------------------------------------
-- Server version	10.1.40-MariaDB-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Device`
--



DROP TABLE IF EXISTS `Device`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Device` (
  `id_device` int(11) NOT NULL AUTO_INCREMENT,
  `address_mac` varchar(50) NOT NULL,
  `address_ip` varchar(40) NOT NULL,
  `date_register` date NOT NULL,
  PRIMARY KEY (`id_device`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Device`
--

LOCK TABLES `Device` WRITE;
/*!40000 ALTER TABLE `Device` DISABLE KEYS */;
/*!40000 ALTER TABLE `Device` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `FileControl`
--

DROP TABLE IF EXISTS `FileControl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `FileControl` (
  `id_fileControl` int(11) NOT NULL AUTO_INCREMENT,
  `time_stamp` datetime NOT NULL,
  `checked_type` varchar(1) NOT NULL,
  `total_hours_day` time NOT NULL,
  `DNI` varchar(9) NOT NULL,
  `id_device` int(11) NOT NULL,
  PRIMARY KEY (`id_fileControl`),
  KEY `FileControl_fk0` (`DNI`),
  KEY `FileControl_fk1` (`id_device`),
  CONSTRAINT `FileControl_fk0` FOREIGN KEY (`DNI`) REFERENCES `User` (`DNI`),
  CONSTRAINT `FileControl_fk1` FOREIGN KEY (`id_device`) REFERENCES `Device` (`id_device`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `FileControl`
--

LOCK TABLES `FileControl` WRITE;
/*!40000 ALTER TABLE `FileControl` DISABLE KEYS */;
/*!40000 ALTER TABLE `FileControl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `User`
--

DROP TABLE IF EXISTS `User`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `User` (
  `DNI` varchar(9) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `password` text NOT NULL,
  `role` varchar(10) NOT NULL,
  `name` varchar(70) NOT NULL,
  `first_name` varchar(70) NOT NULL,
  `last_name` varchar(70) NOT NULL,
  `birth_date` date NOT NULL,
  `phone` varchar(15) NOT NULL,
  `address` varchar(200) NOT NULL,
  `level_studies` varchar(200) NOT NULL,
  `sexo` tinytext NOT NULL,
  `image` blob NOT NULL,
  PRIMARY KEY (`DNI`),
  UNIQUE KEY `user_name` (`user_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `User`
--

LOCK TABLES `User` WRITE;
/*!40000 ALTER TABLE `User` DISABLE KEYS */;
INSERT INTO `User` VALUES ('99998885E','Admin','Admin','A','DEFAULT','ACCOUNT','ADMINISTRATOR','1996-01-01','63136555','sin','sin','H','�PNG\r\n\Z\n\0\0\0\rIHDR\0\0\0�\0\0\0d\0\0\0T���\0\0<IDATx���k\\Eƃ��{�ߠ䣤1j��M�	I\Z�n���t�U�R�M�V�M	��Br�Z�Z���EK)(mM�4��l���MI�\n�Fi�圜]�ǜ�8gf�y����;��xf��fOKK�F�s�zl�*��g[fG�em`$L�m̰!!?a�5�̓�g��Q$TB}7YK\Z��2ʁ��V��Q�&ܣ`��i���\r[$wT0�3���4�yg�X�j��(=��\0���>�}�\r妸�$*��Щ/�ׁ⥝{1�=�m�OR�on�����:�U�+���\'LmSl�2����%�8z�P*,0:�s�\n���c��\'cD�cz\ZRZ���S�,��B\r*�˽���������0h��p�:���Q�`�ҔXt�&������ �].\\���������tw����B00��\0��tf�1�8wZ��\r���i��\"��\0�徨��H�8K���zA�dDp�A���\n`F^~��`�E�`|�Y�``�\0\r�0\0�\0\0`\0��\0�0���@d]�Yw�fq�)�@��\\�n\0.w4��\05�Ji�S@bk�4����Y�S^^0KE�\0���Ȋ���X��B��2p@X\0�4��\0Dp�-V526TX�ˬ�\n*+�\\ɛ*\Z��+\'��6Talp\r`1�r�c��J������{�=Pр�5�x=�C��N���f��:}��tPH���\Z��։3_ֽԓ�Э�i+�/TC���ݍj4q����è�����U[I����Q,~[]=�.��$܆����R�U�����	S�����*ct��\\�!;J�d-Q~KP���;�}�&**!6m{��7�/-�\"K�۰�Mr�|ou��4*��$��ưm���(hX��Ӧ��� �2�rT��~�	k�X�(�%��k��^}S��V�z\0T��=�)��3U�(�ɻ,��\ro?~�Q���iMg��s�IY�Ip�e2y]��mx\\��]���W���4��`���=wQ��FD�$7����4H��,.����h_;�\n���F.F$/0݂]�CG��Hw�f[T`\\�KiHZS�[�\r�H��u�\'\'�w����[?j�/����B\\�1\'���ƭ�$ �C� ���2~y%�.�qdY��Z8�l`X�	ˍ��q�}k���Tf.�dEs`r�)��wKThH���ь��k�O%�D0,.5Ǹ�%~`��4,2�	��5OZX./�u&�\\�-5�#��D�5��gT�����.:�uk:�O)g��������|�`�]�Șt�{m`���,�Ώ��/Q���NK�U��74[�s��$mz�Br�u�L{:s�H�\\���Yiy��y�Ex~h1�Y���%:_�~��?�1m9\nZ��b�=ϟc�\r��y�#��%��E��G��s#�\rj�TX�F4]W�����jw`0m[�R]��{�#�D>#�]Ș�:�vIF�F�2V��t8e��6�\\��?����&�5O2F-N|u���\Z�-�e./���!m�r26]�\r0<Ј��\Z�aqN{uJ�eO�^�dܱ,G:�Y<����	_msYwI��Ӝ��2�����k����p��˪��\0�%@ī�Ƙ\0\0\0\0IEND�B`�');
/*!40000 ALTER TABLE `User` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-06-17 14:25:21
