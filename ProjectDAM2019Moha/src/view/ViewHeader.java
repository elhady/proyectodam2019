package view;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import general.Const;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.Box;
import javax.swing.border.BevelBorder;
import java.awt.Component;
import javax.swing.JPanel;

/**
 * Vista que encapsula las variables genericas del Header.
 */
public class ViewHeader extends ViewBase {

	private static final long serialVersionUID = 1L;
	protected JLabel x_name_user;
	protected JLabel x_image_user;
	protected JLabel x_date;
	protected JLabel x_title;
	protected ViewManager vm;
	protected JLabel x_lb_singOut;
	protected JLabel x_lb_menu;
	private JPanel panel;

	public ViewHeader() {
		this.setSize(Const.PANTALLA_X, Const.PANTALLA_Y);
		setLayout(null);
		panelPrincipal.add(this);
		
		panel = new JPanel();
		panel.setBounds(0, 0, 925, 129);
		add(panel);
		panel.setLayout(null);
		x_name_user = new JLabel("USER_NAME");
		x_name_user.setBounds(156, 5, 500, 23);
		panel.add(x_name_user);
		x_name_user.setFont(new Font("Dubai", Font.PLAIN, 15));
		
				x_title = new JLabel();
				x_title.setBounds(176, 76, 574, 50);
				panel.add(x_title);
				x_title.setHorizontalAlignment(SwingConstants.CENTER);
				x_title.setForeground(new Color(30, 144, 255));
				x_title.setFont(new Font("Times New Roman", Font.BOLD, 30));
				x_title.setText("Titul Finestre");
				x_title.setAutoscrolls(true);
				
						x_date = new JLabel("DATE");
						x_date.setBounds(156, 33, 103, 23);
						panel.add(x_date);
						x_date.setFont(new Font("Dubai", Font.PLAIN, 15));
						
								x_lb_menu = new JLabel("");
								x_lb_menu.setBounds(849, 12, 32, 28);
								panel.add(x_lb_menu);
								x_lb_menu.setIcon(new ImageIcon(ViewHeader.class.getResource("/images/menu.png")));
								
										x_lb_singOut = new JLabel("");
										x_lb_singOut.setBounds(888, 12, 25, 28);
										panel.add(x_lb_singOut);
										x_lb_singOut.setIcon(new ImageIcon(ViewHeader.class.getResource("/images/singout.png")));
										
												Box verticalBox = Box.createVerticalBox();
												verticalBox.setBounds(5, 5, 138, 106);
												panel.add(verticalBox);
												verticalBox.setBorder(new BevelBorder(BevelBorder.LOWERED, new Color(0, 128, 0), new Color(0, 128, 0),
														new Color(0, 128, 0), new Color(0, 128, 0)));
												verticalBox.setBackground(Color.GRAY);
												
														x_image_user = new JLabel("");
														verticalBox.add(x_image_user);
										
												x_lb_singOut.addMouseListener(new MouseAdapter() {
													public void mouseClicked(MouseEvent e) {
														vm.singOut();
													}
												});

		x_lb_menu.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				vm.returnToMenu();
			}
		});

	}
}
