package view;

import java.awt.Button;
import java.awt.Color;
import java.awt.Font;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import general.Common;
import general.Const;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Dimension;
import javax.swing.SwingConstants;

/**
 * Vista que permite registrar entradas y salidas.
 */

public class Fichar extends ViewHeader {

	private static final long serialVersionUID = 1L;

	public Fichar(ViewManager vm) {
			this.vm = vm;
		super.vm = vm;

		this.setSize(new Dimension(944, 465));
		this.setBackground(Color.WHITE);

		this.x_title.setText("Registrar entrada / salida");
		this.x_date.setText("");

		DateFormat outputformat = new SimpleDateFormat("HH:mm:ss - MM/dd/yyyy");
		setLayout(null);

		JLabel x_hora_fichada = new JLabel(outputformat.format(new Date()));
		x_hora_fichada.setHorizontalAlignment(SwingConstants.CENTER);
		x_hora_fichada.setBounds(12, 180, 909, 57);
		x_hora_fichada.setFont(new Font("Dubai", Font.BOLD, 52));
		Timer timer = new Timer();
		timer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				x_hora_fichada.setText(Common.TimeFormat.format(new Date()));
			}
		}, 0, 1000);
		this.add(x_hora_fichada);

		JLabel x_fecha_fichada = new JLabel(outputformat.format(new Date()));
		x_fecha_fichada.setHorizontalAlignment(SwingConstants.CENTER);
		x_fecha_fichada.setBounds(12, 233, 909, 57);
		x_fecha_fichada.setFont(new Font("Dialog", Font.BOLD, 22));

		timer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				x_fecha_fichada.setText(new SimpleDateFormat("MM/dd/yyyy").format(new Date()));

			}
		}, 0, 1000);
		this.add(x_fecha_fichada);

		Button btn_fichar = new Button("Fichar");
		btn_fichar.setForeground(new Color(255, 255, 255));
		btn_fichar.setFont(new Font("Dubai", Font.BOLD, 26));
		btn_fichar.setBounds(282, 359, 380, 45);
		btn_fichar.setBackground(new Color(46, 139, 87));
		btn_fichar.setActionCommand("Fichar");
		this.add(btn_fichar);
		btn_fichar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				vm.eventFichar(new Date(), vm.getUserLoged());
			}
		});

	}

	/**
	 * Método que tipifica el tipo de registro realizado al fichar.
	 * 
	 * @param typeRegister
	 * @param insertOk
	 */
	public void ficharResult(char typeRegister, boolean insertOk) {
		if (!insertOk) {
			String type = (typeRegister == Const.TYPE_ENTRY) ? "entrada" : "salida";
			JOptionPane.showMessageDialog(vm.mainFrame, "Se ha registrdo la " + type + " correctamente!", "",
					JOptionPane.INFORMATION_MESSAGE);
			vm.returnToMenu();

		} else {
			JOptionPane.showMessageDialog(vm.mainFrame, "Error al intentar registar la entrada / salida", "",
					JOptionPane.WARNING_MESSAGE);
		}

	}
}
