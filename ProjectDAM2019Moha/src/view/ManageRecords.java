package view;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;

import general.Common;
/**
 * Vista de gestión de entradas y salidas.
 * Permite consultrar, Modficar y insertar nuevos registros manualmente.
 * 
 * @author elhady
 *
 */
public class ManageRecords extends ViewHeader {

	private static final long serialVersionUID = 1L;
	private ViewManager vm;
	private JFormattedTextField x_desde_fecha;
	private JFormattedTextField x_hasta_fecha;
	private JTextField x_in_tipoReg;
	private JTextField x_in_hora;
	private JTextField x_in_fecha;
	private JTextField x_in_numReg;
	protected JTable table;
	protected DefaultTableModel model = null;

	public ManageRecords(ViewManager vm) {
		this.vm = vm;
		super.vm = vm;
		this.x_title.setText("Gestionar registros");
		this.setSize(super.getSize());
		this.setBackground(Color.WHITE);

		JButton btnAadir = new JButton("Añadir");
		btnAadir.setBounds(667, 406, 117, 25);
		add(btnAadir);

		JButton btnModificar = new JButton("Modificar");
		btnModificar.setEnabled(false);
		btnModificar.setBounds(796, 406, 117, 25);
		add(btnModificar);

		JButton btnActualizar = new JButton("Actualizar");
		btnActualizar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnActualizar.setBounds(539, 406, 117, 25);
		add(btnActualizar);

		x_desde_fecha = Common.getJFormattedTextField();
		x_desde_fecha.setToolTipText("Fecha");
		x_desde_fecha.setColumns(10);
		x_desde_fecha.setBounds(139, 149, 147, 28);
		add(x_desde_fecha);

		JLabel label_4 = new JLabel("Desde fecha");
		label_4.setBounds(12, 148, 124, 28);
		add(label_4);

		x_hasta_fecha = Common.getJFormattedTextField();
		x_hasta_fecha.setToolTipText("Fecha");
		x_hasta_fecha.setColumns(10);
		x_hasta_fecha.setBounds(428, 148, 147, 28);
		add(x_hasta_fecha);
		x_hasta_fecha.setText(Common.getDateFormatST(new Date(), Common.dateFormt));

		JLabel label_5 = new JLabel("Hasta fecha");
		label_5.setBounds(301, 147, 124, 28);
		add(label_5);

		x_in_tipoReg = new JTextField();
		x_in_tipoReg.setEnabled(false);
		x_in_tipoReg.setToolTipText("Tipo de resgistro");
		x_in_tipoReg.setColumns(10);
		x_in_tipoReg.setBounds(144, 505, 168, 28);
		add(x_in_tipoReg);

		JLabel label = new JLabel("Tipo registro");
		label.setBounds(12, 505, 130, 28);
		add(label);

		JLabel label_1 = new JLabel("Hora");
		label_1.setBounds(336, 505, 130, 28);
		add(label_1);

		x_in_hora = new JTextField();
		x_in_hora.setEnabled(false);
		x_in_hora.setToolTipText("Hora");
		x_in_hora.setColumns(10);
		x_in_hora.setBounds(468, 505, 168, 28);
		add(x_in_hora);

		x_in_fecha = Common.getJFormattedTextField();
		x_in_fecha.setEnabled(false);
		x_in_fecha.setToolTipText("Fecha");
		x_in_fecha.setColumns(10);
		x_in_fecha.setBounds(468, 470, 168, 28);
		add(x_in_fecha);

		JLabel label_2 = new JLabel("Fecha");
		label_2.setBounds(336, 470, 130, 28);
		add(label_2);

		JLabel label_3 = new JLabel("Número registro");
		label_3.setBounds(12, 470, 130, 28);
		add(label_3);

		x_in_numReg = new JTextField();
		x_in_numReg.setEnabled(false);
		x_in_numReg.setToolTipText("Número de registro");
		x_in_numReg.setColumns(10);
		x_in_numReg.setBounds(144, 470, 168, 28);
		add(x_in_numReg);

		JButton btn_aceptar_fechas = new JButton("Aceptar");
		btn_aceptar_fechas.setBounds(592, 150, 117, 25);
		add(btn_aceptar_fechas);

		JButton btn_aceptar_new = new JButton("Aceptar");
		btn_aceptar_new.setEnabled(false);
		btn_aceptar_new.setBounds(648, 471, 117, 25);
		add(btn_aceptar_new);

		JButton btn_cancelar_new = new JButton("Cancelar");
		btn_cancelar_new.setEnabled(false);
		btn_cancelar_new.setBounds(648, 506, 117, 25);
		add(btn_cancelar_new);

		JButton btn_imprimir = new JButton("Imprimir");
		btn_imprimir.setEnabled(false);
		btn_imprimir.setBounds(13, 406, 117, 25);
		add(btn_imprimir);

		String data[][] = {};
		String col[] = { "N\u00FAmero registro", "Fecha", "Hora", "Tipo de registro" };

		model = new DefaultTableModel(data, col) {// Model No editable
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};

		table = new JTable() {// Table con las comunas centradas
			private static final long serialVersionUID = 1L;

			@Override
			public Component prepareRenderer(TableCellRenderer renderer, int row, int col) {
				Component comp = super.prepareRenderer(renderer, row, col);
				((JLabel) comp).setHorizontalAlignment(JLabel.CENTER);
				return comp;
			}
		};

		table.setFont(new Font("Dialog", Font.PLAIN, 13));
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setModel(model);

		JScrollPane pane = new JScrollPane(table);
		pane.setBounds(12, 189, 901, 213);
		add(pane);

		btn_aceptar_fechas.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String dateIn = x_desde_fecha.getText();
				String dateFi = x_hasta_fecha.getText();
				vm.eventGetRegisters(dateIn, dateFi);
			}
		});
	}
}
