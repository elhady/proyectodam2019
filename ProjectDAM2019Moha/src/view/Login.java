package view;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JTextPane;
import general.Common;
import general.Const;

/**
 * Vista que permite hacer el login.
 */

public class Login extends ViewBase {

	private static final long serialVersionUID = 1L;
	protected JTextField x_user;
	protected JPasswordField x_passwd;
	protected JButton x_btn_iniciar;
	ViewManager vm;

	public Login(ViewManager vm) {
		this.vm = vm;
		this.setSize(super.getSize());
		this.setBackground(Color.WHITE);
		this.setSize(Const.PANTALLA_X, Const.PANTALLA_Y);

		JPanel panel = new JPanel();
		panel.setBackground(Color.DARK_GRAY);
		panel.setBounds(0, 0, 346,  Const.PANTALLA_Y);
		this.add(panel);
		panel.setLayout(null);

		JLabel label = new JLabel("");

		label.setBounds(-28, 250, 420, 256);
		label.setVerticalAlignment(SwingConstants.TOP);
		label.setIcon(new ImageIcon(Login.class.getResource("/images/loginPhoto.png")));
		panel.add(label);

		JLabel lblLogo = new JLabel("");
		lblLogo.setBounds(68, 30, 208, 166);
		panel.add(lblLogo);
		lblLogo.setIcon(new ImageIcon(Login.class.getResource("/images/logoName.png")));

		x_btn_iniciar = new JButton("Iniciar");
		x_btn_iniciar.setActionCommand("Iniciar");
		x_btn_iniciar.setFont(new Font("Segoe UI", Font.PLAIN, 20));
		x_btn_iniciar.setForeground(Color.WHITE);
		x_btn_iniciar.setBackground(new Color(241, 57, 83));
		x_btn_iniciar.setBounds(504, 320, 283, 36);
		this.add(x_btn_iniciar);
		/**
		 * Método que llama a función 'comprobar login'
		 */
		x_btn_iniciar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String userName = x_user.getText();
				String pass = String.valueOf(x_passwd.getPassword());

				if (!Common.isEmpty(userName) && !Common.isEmpty(pass)) {
					vm.eventValidaLogin(userName, pass);

				} else {
					JOptionPane.showMessageDialog(vm.mainFrame, "No se ha informado el Nombre de usuario o Contraseña",
							"", JOptionPane.WARNING_MESSAGE);
				}
			}
		});

		x_user = new JTextField();
		x_user.setBounds(504, 190, 283, 36);
		this.add(x_user);
		x_user.setColumns(10);

		JLabel lblUsername = new JLabel("Nombre de usuario");
		lblUsername.setFont(new Font("Segoe UI", Font.PLAIN, 15));
		lblUsername.setBounds(504, 165, 283, 21);
		this.add(lblUsername);

		JLabel lblPassword = new JLabel("Contrase\u00F1a");
		lblPassword.setFont(new Font("Segoe UI", Font.PLAIN, 15));
		lblPassword.setBounds(504, 240, 283, 21);
		this.add(lblPassword);

		x_passwd = new JPasswordField();
		x_passwd.setBounds(504, 264, 283, 36);
		this.add(x_passwd);

		JTextPane xtitle = new JTextPane();
		xtitle.setEditable(false);
		xtitle.setForeground(new Color(250, 128, 114));
		xtitle.setFont(new Font("Times New Roman", Font.BOLD, 32));
		xtitle.setText("Iniciar sessi\u00F3n");
		xtitle.setBounds(500, 76, 283, 49);
		this.add(xtitle);
		this.setVisible(true);

	}

	public void loginFailed() {
		JOptionPane.showMessageDialog(this, "Bad userName or password!", "", JOptionPane.ERROR_MESSAGE);
	}
}
