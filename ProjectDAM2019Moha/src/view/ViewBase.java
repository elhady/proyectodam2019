package view;

import javax.swing.JPanel;
import general.Const;

/**
 * Vista base que hereda de JPanel.
 */

public class ViewBase extends JPanel {

	private static final long serialVersionUID = 1L;
	JPanel panelPrincipal;

	public ViewBase pantallaAnterior;

	public ViewBase() {
		panelPrincipal = new JPanel();
		panelPrincipal.setSize(Const.PANTALLA_X, Const.PANTALLA_Y);
		this.setSize(Const.PANTALLA_X, Const.PANTALLA_Y);

		panelPrincipal.setVisible(true);
		panelPrincipal.setLayout(null);
	}

	public void mostra(boolean value) {
		setVisible(value);
	}

	public void setPanel(JPanel p) {
		this.panelPrincipal = p;
	}
}
