package view;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import controller.Controller;

import java.awt.Button;
import javax.swing.UIManager;
import javax.swing.border.BevelBorder;
/**
 * Vista del menú principal.
 */
public class Menu extends ViewHeader {

	private static final long serialVersionUID = 1L;
	ViewManager vm;

	public Menu(ViewManager vm) {
		
		this.vm = vm;
		super.vm = vm;
		this.x_title.setText("Menú");
		this.x_lb_menu.setVisible(false);;

		this.setSize(vm.mainFrame.getSize());
		this.setBackground(Color.WHITE);
		setLayout(null);

		Button x_btn_fichar = new Button("Fichar");
		x_btn_fichar.setBounds(317, 224, 277, 42);
		x_btn_fichar.setBackground(new Color(169, 169, 169));
		x_btn_fichar.setFont(new Font("Segoe UI", Font.PLAIN, 25));
		this.add(x_btn_fichar);

		Button x_btn_consultar = new Button("Gestionar registros");
		x_btn_consultar.setBounds(317, 281, 277, 42);
		x_btn_consultar.setActionCommand("Consultar");
		x_btn_consultar.setFont(new Font("Segoe UI", Font.PLAIN, 25));
		x_btn_consultar.setBackground(new Color(169, 169, 169));
		this.add(x_btn_consultar);

		Button x_btn_new_user = new Button("Crear usuario");
		x_btn_new_user.setBounds(317, 339, 277, 42);
		x_btn_new_user.setFont(new Font("Segoe UI", Font.PLAIN, 25));
		x_btn_new_user.setBackground(new Color(169, 169, 169));
		this.add(x_btn_new_user);

		Button x_btn_validar = new Button("Validar horas");
		x_btn_validar.setBounds(317, 396, 277, 42);
		x_btn_validar.setFont(new Font("Segoe UI", Font.PLAIN, 25));
		x_btn_validar.setBackground(new Color(169, 169, 169));
		this.add(x_btn_validar);
		
		
		 /**
		  * Si no es un administrador no puede crear usuarios ni validar horas
		  */
		if(!vm.getUserLoged().isAdmin()) {
			x_btn_validar.setVisible(false);
			x_btn_new_user.setVisible(false);
			x_btn_fichar.setLocation(x_btn_fichar.getX(), x_btn_fichar.getY() + 70);
			x_btn_consultar.setLocation(x_btn_consultar.getX(), x_btn_consultar.getY() + 70);;
		}
		
		/**
		 * Método de llama a la función 'fichar'
		 */
		x_btn_fichar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {

				vm.printView(vm.fichar = new Fichar(vm));
			}
		});

		/**
		 * Método de llama a la función 'añadir usuario'
		 */
		x_btn_new_user.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				vm.printView(vm.addUser = new AddUser(vm));
			}
		});
		
		x_btn_consultar.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				vm.printView(vm.manageRecords = new ManageRecords(vm));
				
			}
		});
		x_date.setText(vm.dateHeader);

	}

}
