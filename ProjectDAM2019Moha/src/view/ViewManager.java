package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import java.io.IOException;
import javax.swing.JOptionPane;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.Timer;
import java.util.TimerTask;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import general.Common;
import general.Const;
import model.FileControl;
import model.User;

/**
 * Clase que controla todas las vistas.
 */

public class ViewManager extends Observable { // observable

	protected Login login;
	protected ViewBase menu;
	protected Fichar fichar = null;
	protected AddUser addUser = null;
	protected User userLoged = null;
	protected JFrame mainFrame = null;
	protected String dateHeader = null;
	protected ManageRecords manageRecords = null;

	public Login getLogin() {
		return login;
	}

	public void setLogin(Login login) {
		this.login = login;
	}

	public ViewBase getMenu() {
		return menu;
	}

	public void setMenu(ViewBase menu) {
		this.menu = menu;
	}

	public Fichar getFichar() {
		return fichar;
	}

	public void setFichar(Fichar fichar) {
		this.fichar = fichar;
	}

	public AddUser getAddUser() {
		return addUser;
	}

	public void setAddUser(AddUser addUser) {
		this.addUser = addUser;
	}

	public String getDateHeader() {
		return dateHeader;
	}

	public void setDateHeader(String dateHeader) {
		this.dateHeader = dateHeader;
	}

	/**
	 * Método que crea y muestra un JFrame con la view Login
	 */
	public void init() {
		mainFrame = new JFrame("ControlTime");
		Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
		mainFrame.setSize(Const.PANTALLA_X, Const.PANTALLA_Y);
		int x = (int) ((dimension.getWidth() - mainFrame.getWidth()) / 2);
		int y = (int) ((dimension.getHeight() - mainFrame.getHeight()) / 4);
		mainFrame.setLocation(x, y);
		mainFrame.setIconImage(Toolkit.getDefaultToolkit().getImage(ViewManager.class.getResource("/images/logo.png")));
		mainFrame.setTitle("ControlTime");
		mainFrame.setBackground(Color.WHITE);
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainFrame.setVisible(true);
		mainFrame.setResizable(false);
		login = new Login(this);

		printView(login);
	}

	/**
	 * Método que pinta las pantallas en el JFrame y en caso de que la vista herede
	 * de ViewHeader carga las variables del HEADER.
	 * 
	 * @param view vista que se deasea pintar
	 */
	public void printView(JPanel view) {
		// Si tiene header si cargan sus variables
		if (view instanceof ViewHeader) {
			cargarDatosHeader(getUserLoged(), (ViewHeader) view);
		}

		view.setVisible(true);
		view.setLayout(null);
		mainFrame.setLayout(null);

		mainFrame.setContentPane(view);
	}

	/**
	 * Método que notifica al observer que se ha intentado hacer un login y le pasa
	 * el usuario y contraseña.
	 * 
	 * @param user
	 * @param pass
	 */
	public void eventValidaLogin(String user, String pass) {
		try {
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("event", Const.EV_LOGIN);
		data.put("user", user);
		data.put("pass", pass);

		// generate the event
		setChanged();
		notifyObservers(data);
		}catch (Exception e) {
		
		JOptionPane.showMessageDialog(this.login, "No se ha podido realizar la conexión con la base de datos",
				"", JOptionPane.ERROR_MESSAGE);
		}
	}

	public void eventGetRegisters(String dateIn, String dateFi) {
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("event", Const.EV_REGISTERS);
		data.put("user", userLoged);
		data.put("dateIn", dateIn);
		data.put("dateFi", dateFi);

		// generate the event
		setChanged();
		notifyObservers(data);
	}

	/**
	 * Método que notifica al observer que de ha generado un evento fichar y le pasa
	 * la hora y el usuario.
	 * 
	 * @param dateTime
	 * @param user
	 */
	public void eventFichar(Date dateTime, User user) {
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("event", Const.EV_FICHAR );
		data.put("user", user);
		data.put("DateLogin", dateTime);

		// generate the event
		setChanged();
		notifyObservers(data);
	}

	/**
	 * Método que notifica al observer que de desea crear un usuario nuevo le pasa
	 * el usuario que se deasea crear.
	 * 
	 * @param user
	 */
	public void eventAddUser(User user) {
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("event", Const.EV_ADDUSER);
		data.put("user", user);

		// generate the event
		setChanged();
		notifyObservers(data);
	}

	public User getUserLoged() {
		return userLoged;
	}

	/**
	 * Método que devuelve el usuario logeado.
	 * 
	 * @param userLoged
	 */
	public void setUserLoged(User userLoged) {
		this.userLoged = userLoged;
	}

	/**
	 * Método que espera la respuesta del intento de login. Si existe el usuario que
	 * se ha intentado loguear se crear y muestra el menu, sino muestra un mensaje
	 * de error.
	 * 
	 * @param user
	 */
	public void loginResult(User user) {
		if (Common.isEmpty(user)) {
			login.loginFailed();

		} else {
			setUserLoged(user);
			this.menu = new Menu(this);
			printView(menu);
		}
	}

	/**
	 * Método que se encarga de llenar la tabla de registros (Entradas y salidas )
	 * 
	 * @param listFC
	 */
	public void insertRegistersToTable(ArrayList<FileControl> listFC) {
		limpiarTable();

		if (Common.isEmpty(listFC) || listFC.size() == 0) {
			JOptionPane.showMessageDialog(manageRecords,
					"No se han encontrado registros con los criterios proporcionados!", "", JOptionPane.WARNING_MESSAGE);
		} else {
			for (int i = 0; i < listFC.size(); i++) {
				Object[] data = { listFC.get(i).getId(),
						Common.getDateFormatST(listFC.get(i).getTimeStamp(), Common.dateFormt),
						Common.getDateFormatST(listFC.get(i).getTimeStamp(), Common.TimeFormat),
						(listFC.get(i).getCheckedType().charAt(0) == Const.TYPE_ENTRY) ? "Entrada" : "Salida" };

				manageRecords.model.addRow(data);
			}
		}
	}

	public void limpiarTable() {
		try {
			int filas = manageRecords.table.getRowCount();
			for (int i = 0; filas > i; i++) {
				manageRecords.model.removeRow(0);
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Error al limpiar la tabla.");
		}
	}

	public void addUserResult(boolean result) {
		addUser.addUserResult(result);
	}

	public void ficharResult(char typeRegister, boolean result) {
		fichar.ficharResult(typeRegister, result);
	}

	public void returnToMenu() {
		printView(this.menu);
	}

	public void singOut() {
		printView(new Login(this));
	}

	/**
	 * Método que recibe un usuario y una vista que hereda del ViewHeader y pone el
	 * nombre,apellidos y foto del usuario en el header
	 * 
	 * @param user
	 * @param view
	 */
	public void cargarDatosHeader(User user, ViewHeader view) {
		if (!Common.isEmpty(menu)) {

			Timer timer = new Timer();
			timer.scheduleAtFixedRate(new TimerTask() {
				@Override
				public void run() {
					view.x_date.setText(new SimpleDateFormat("HH:mm:ss").format(new Date()));

				}
			}, 0, 1000);

			view.x_name_user.setText(Common.getCompletNameUser(user));

			try {

				Image image = ImageIO.read(user.getImage().getBinaryStream());
				if (!Common.isEmpty(image))
					view.x_image_user.setIcon(new ImageIcon(image));
			} catch (IOException | SQLException e1) {
				e1.printStackTrace();
			}
		}
	}

}
