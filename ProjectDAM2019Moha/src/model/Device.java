package model;

import java.util.Date;

/**
 * Clase modelo del dispositivo.
 * @author elhady
 *
 */
public class Device {
private int id;//TODO
private Date dateRegister;
private String MAC;
private String IP;
public Device(int id, Date dateRegister, String mAC, String iP) {
	super();
	this.id = id;
	this.dateRegister = dateRegister;
	MAC = mAC;
	IP = iP;
}

public Device(Date dateRegister, String mAC, String iP) {
	super();
	//this.id = id;
	this.dateRegister = dateRegister;
	MAC = mAC;
	IP = iP;
}
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public Date getDateRegister() {
	return dateRegister;
}
public void setDateRegister(Date dateRegister) {
	this.dateRegister = dateRegister;
}
public String getMAC() {
	return MAC;
}
public void setMAC(String mAC) {
	MAC = mAC;
}
public String getIP() {
	return IP;
}
public void setIP(String iP) {
	IP = iP;
}
}
