package model;

import java.sql.Time;
import java.util.Date;

/**
 * Clase modelo de la Fichada.
 * 
 * @author elhady
 *
 */
public class FileControl {
	private int id;
	private Date timeStamp;
	private String checkedType;
	private Time totalHoursDay; // TODO
	private String DNI;
	private int idDevice;

	public FileControl(int id, Date timeStamp, String checkedType, Time totalHoursDay, String dNI, int idDevice) {
		super();
		this.id = id;
		this.timeStamp = timeStamp;
		this.checkedType = checkedType;
		this.totalHoursDay = totalHoursDay;
		DNI = dNI;
		this.idDevice = idDevice;
	}

	public FileControl() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getCheckedType() {
		return checkedType;
	}

	public void setCheckedType(String checkedType) {
		this.checkedType = checkedType;
	}

	public Time getTotalHoursDay() {
		return totalHoursDay;
	}

	public void setTotalHoursDay(Time totalHoursDay) {
		this.totalHoursDay = totalHoursDay;
	}

	public String getDNI() {
		return DNI;
	}

	public void setDNI(String dNI) {
		DNI = dNI;
	}

	public int getIdDevice() {
		return idDevice;
	}

	public void setIdDevice(int idDevice) {
		this.idDevice = idDevice;
	};

}
