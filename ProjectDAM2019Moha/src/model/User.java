package model;

import java.util.Date;

/**
 * Clase modelo del usuario.
 * @author elhady
 *
 */
public class User {
private String DNI;
private String userName; 
private String password;
private String lastName;
private String firstName;
private String  name;	
private Date birthDate;
private String phone;
private String address;
private String levelStudies;
private String sexo;
private String role;
private java.sql.Blob image;

private final String ADMIN = "A";

	// | DNI | user_name | password | role | name | first_name | last_name |
	// birth_date | phone | address      | level_studies      | sexo | image |

public User(String DNI, String userName, String password, String role, String name, String firstName, String lastName,
		Date birthDate, String phone, String address, String levelStudies, String sexo, java.sql.Blob image) {
	super();
	this.DNI = DNI;
	this.userName = userName;
	this.password = password;
	this.lastName = lastName;
	this.firstName = firstName;
	this.name = name;
	this.birthDate = birthDate;
	this.phone = phone;
	this.address = address;
	this.levelStudies = levelStudies;
	this.sexo = sexo;
	this.role = role;
	this.image = image;
	
}
public String getDNI() {
	return DNI;
}
public void setDNI(String dNI) {
	DNI = dNI;
}
public String getUserName() {
	return userName;
}
public void setUserName(String userName) {
	this.userName = userName;
}
public String getPassword() {
	return password;
}
public void setPassword(String password) {
	this.password = password;
}
public String getLastName() {
	return lastName;
}
public void setLastName(String lastName) {
	this.lastName = lastName;
}
public String getFirstName() {
	return firstName;
}
public void setFirstName(String firstName) {
	this.firstName = firstName;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public Date getBirthDate() {
	return birthDate;
}
public void setBirthDate(Date birthDate) {
	this.birthDate = birthDate;
}
public String getPhone() {
	return phone;
}
public void setPhone(String phone) {
	this.phone = phone;
}
public String getAddress() {
	return address;
}
public void setAddress(String address) {
	this.address = address;
}
public String getLevelStudies() {
	return levelStudies;
}
public void setLevelStudies(String levelStudies) {
	this.levelStudies = levelStudies;
}
public String getRole() {
	return role;
}
public void setRole(String role) {
	this.role = role;
}
public java.sql.Blob getImage() {
	return image;
}
public void setImage(java.sql.Blob image) {
	this.image = image;
}

public String getSexo() {
	return sexo;
}
public void setSexo(String sexo) {
	this.sexo = sexo;
}

public boolean isAdmin() {
	return (ADMIN.equalsIgnoreCase(getRole()));
}

}
