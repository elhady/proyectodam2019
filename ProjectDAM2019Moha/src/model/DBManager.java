package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import general.Common;
import general.ConnectionManager;
import general.Const;

/**
 * Clase que interactua con la base de datos.
 * 
 * @author elhady
 *
 */

public class DBManager {

	public DBManager() throws SQLException {
	};

//	public ArrayList<FileControl> getFichadas(Date fDesde, Date fHasta) {
//		ArrayList<FileControl> fichadas = new ArrayList<FileControl>();
//		Connection conn = null;
//		Statement s = null;
//		ResultSet rs = null;
//		try {
//			conn = ConnectionManager.getConnection();
//			s = conn.createStatement();
//			String sql;
//
//			// rs = s.executeQuery(sql);
//			if (rs.next()) {
//
//			}
//
//			conn.close();
//		} catch (Exception e) {
//			System.err.println("Got an exception!");
//			System.err.println(e.getMessage());
//
//		} finally {
//			try {
//				if (s != null)
//					s.close();
//				if (rs != null)
//					rs.close();
//				if (conn != null)
//					conn.close();
//
//			} catch (SQLException e) {
//				e.printStackTrace();
//				System.err.println(e.getMessage());
//			}
//		}
//
//		return null;
//
//	}

	// String DNI, String userName, String password, String lastName, String
	// firstName, String name,
	// Date birthDate, String phone, String address, String levelStudies, String
	// sexo, String role, java.sql.Blob image
	/*
	 * public boolean crearUsuario(User u) throws SQLException { Connection conn =
	 * ConnectionManager.getConnection(); java.sql.Statement st = null; boolean
	 * result = false; try { st = conn.createStatement();
	 * 
	 * String sql = " INSERT INTO User  VALUES( " // TODO // ORGANIZACIÓN +
	 * u.getDNI() + "," + u.getUserName() + "," + u.getPassword() + "," +
	 * u.getLastName() + "," + u.getFirstName() + "," + u.getName() + "," +
	 * u.getBirthDate() + "," + u.getPhone() + "," + u.getAddress() + "," +
	 * u.getLevelStudies() + "," + u.getSexo() + "," + u.getRole() + "," +
	 * u.getImage() + ")";
	 * 
	 * result = st.execute(sql);
	 * 
	 * } finally { st.close(); conn.close();
	 * 
	 * }
	 * 
	 * return result; }
	 */

	// TODO calcular total //
	// Mostrar cuantas horas
	// lleva trabajadas en
	// la jornada
	// horas
	// TODO comprobar si el Device es el habituals

	/**
	 * Método que hace un insert en la tabla de file control.'lo que equivale una
	 * una fichada'
	 * 
	 * @param User   user
	 * @param Device device
	 * @param Date   dateTime
	 * @param Char   type
	 * @return boolean result
	 */
	public boolean fichar(User user, Device device, Date dateTime, char type) {
		boolean result = false;
		Connection conn = null;
		Statement s = null;
		ResultSet rs = null;

		try {
			conn = ConnectionManager.getConnection();
			s = conn.createStatement();
			int idDevice = insertDevice(device);

			String query = " INSERT INTO FileControl(time_stamp,checked_type, total_hours_day,DNI,id_device)"
					+ " values ( ?, ?,?,?,?)";

			PreparedStatement preparedStmt = conn.prepareStatement(query);
			preparedStmt.setString(1, Common.dateTimeFormatSQL.format(dateTime));
			preparedStmt.setString(2, String.valueOf(type));
			preparedStmt.setTime(3, new Time(new Date().getTime()));
			preparedStmt.setString(4, user.getDNI());
			preparedStmt.setInt(5, idDevice);

			result = preparedStmt.execute();

		} catch (Exception e) {
			System.err.println(e.getMessage());

		} finally {
			try {
				if (s != null)
					s.close();
				if (rs != null)
					rs.close();
				if (conn != null)
					conn.close();

			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return result;
	}

	/**
	 * Método que hace el insert del despositivo en la tabla Device
	 * 
	 * @param device
	 * @return int Id del despositivo
	 */
	public int insertDevice(Device device) {
		int result = 0;
		Connection conn = null;
		Statement s = null;
		ResultSet rs = null;
		try {
			conn = ConnectionManager.getConnection();
			s = conn.createStatement();
			String sql;
			String query = " insert into Device(address_mac, address_ip, date_register)" + " values ( ?, ?, ?)";

			PreparedStatement preparedStmt = conn.prepareStatement(query);
			preparedStmt.setString(1, device.getMAC());
			preparedStmt.setString(2, device.getIP());
			preparedStmt.setDate(3, Common.getDateFormatSQL(device.getDateRegister()));
			preparedStmt.execute();

			sql = "select MAX(id_device) as id FROM Device";
			rs = s.executeQuery(sql);

			if (rs.next()) {
				result = rs.getInt(1);
			}

			conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception!");
			System.err.println(e.getMessage());

		} finally {
			try {
				if (s != null)
					s.close();
				if (rs != null)
					rs.close();
				if (conn != null)
					conn.close();

			} catch (SQLException e) {
				e.printStackTrace();
				System.err.println(e.getMessage());
			}
		}
		return result;
	}

	/**
	 * Método que devuelve un arraylist con las ficadas que cumplen las
	 * restricciónes indicadas en los parámetros.
	 * 
	 * @param User usuario
	 * @param Date desdeFecha
	 * @param Date hastaFecha
	 * @return ArrayList<FileControl>
	 * @throws SQLException
	 */
	public static ArrayList<FileControl> getRegistrsFileControl(User usuario, Date desdeFecha, Date hastaFecha) {
		ArrayList<FileControl> listFileControl = new ArrayList<>();
		String sql = null;
		java.sql.Statement st = null;

		Connection conn = null;
		Statement s = null;
		ResultSet rs = null;

		try {
			conn = ConnectionManager.getConnection();
			s = conn.createStatement();

			sql = "SELECT * from FileControl  ";
			sql += "WHERE ";
			sql += " DNI = '" + usuario.getDNI() + "'";
			if (!Common.isEmpty(desdeFecha)) {
				sql += " AND time_stamp >= '" + new java.sql.Date(desdeFecha.getTime()) + " 00:00:00'";
			}

			if (!Common.isEmpty(hastaFecha)) {
				sql += " AND time_stamp <= '" + new java.sql.Date(hastaFecha.getTime()) + " 23:59:59'";
			}
			sql += " ORDER BY time_stamp DESC ";

			rs = s.executeQuery(sql);
			while (rs.next()) {
				listFileControl.add(new FileControl(rs.getInt(1), new java.util.Date(rs.getDate(2).getTime()),
						rs.getString(3), rs.getTime(4), rs.getString(5), rs.getInt(6)));
			}

		} catch (Exception e) {
			System.err.println("Got an exception!");
			System.err.println(e.getMessage());
		} finally {
			try {
				if (s != null)
					s.close();
				if (rs != null)
					rs.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				System.err.println(e.getMessage());
			}
		}

		return listFileControl;
	}

	/**
	 * Método que se encarga de comprobar si existe el usuario y si la contraseña
	 * coincide.
	 * 
	 * @param String userName
	 * @param String password
	 * @return boolean result
	 */
	public User validaLogin(String userName, String password) {// TODO ENCRIPT AND DES
		User user = null;
		// | DNI | user_name | password | role | name | first_name | last_name |
		// birth_date | phone | address | level_studies | sexo | image |

		try {

			Connection conn = ConnectionManager.getConnection();
			Statement s = conn.createStatement();
			ResultSet rs;

			String sql = "Select * from User WHERE user_name = '" + userName + "'";

			rs = s.executeQuery(sql);
			if (rs.next()) {
				if (password.equals(rs.getString("password"))) {
					user = new User(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5),
							rs.getString(6), rs.getString(7), rs.getDate(8), rs.getString(9), rs.getString(10),
							rs.getString(11), rs.getString(12), rs.getBlob(13));
				}
			}
			rs.close();
			conn.close();

		} catch (SQLException sqle) {
			JOptionPane.showMessageDialog(null, "Error en la consulta MySQL");
			Logger.getLogger(DBManager.class.getName()).log(Level.SEVERE, null, sqle);
		}
		return user;
	}

	/**
	 * Método que devuelve el tipo de registro(Entrada / Salida ) de la Próxima
	 * fichada de un usuario
	 * 
	 * @param User user
	 * @return char tipo de fichada 'E/S'
	 */
	public char getTypeRegister(User user) {
		char result = 0;

		try {
			Connection conn = ConnectionManager.getConnection();
			Statement s = conn.createStatement();
			ResultSet rs;

			String sql = "SELECT (count(1) % 2) AS type from FileControl WHERE DNI =  '" + user.getDNI() + "'";
			rs = s.executeQuery(sql);

			if (rs.next()) {
				result = (rs.getInt(1) == 0) ? Const.TYPE_ENTRY : Const.TYPE_EXIT;
			}

			rs.close();
			conn.close();
		} catch (SQLException sqle) {
			JOptionPane.showMessageDialog(null, "Error en la consulta MySQL");
			Logger.getLogger(DBManager.class.getName()).log(Level.SEVERE, null, sqle);
		}

		return result;
	}

	/**
	 * Método que comprueba si existe el usuario antes de intentar hacer el insert.
	 * 
	 * @param User user
	 * @return boolean result
	 */
	public boolean isExistUser(User user) {
		// TODO
		return false;
	}

	/**
	 * Método que se encarga de hacer el insert en la tabla de 'user'
	 * 
	 * @param User user
	 * @return boolean ok/ko
	 */
	public boolean insertUser(User user) {
		boolean result = false;
		Connection conn = null;
		Statement s = null;
		ResultSet rs = null;

		try {
			conn = ConnectionManager.getConnection();
			s = conn.createStatement();

			// | DNI | user_name | password | role | name | first_name |
			// last_name |
			// birth_date | phone | address | level_studies | sexo | image |

			String query = " INSERT INTO User(DNI, user_name, password, role, name, first_name, last_name, birth_date, phone, address, level_studies, sexo, image)"
					+ " values ( ?,?,?,?,?,?,?,?,?,?,?,?,?)";

			PreparedStatement preparedStmt = conn.prepareStatement(query);
			preparedStmt.setString(1, user.getDNI());
			preparedStmt.setString(2, user.getUserName());
			preparedStmt.setString(3, user.getPassword());
			preparedStmt.setString(4, user.getRole());
			preparedStmt.setString(5, user.getName());
			preparedStmt.setString(6, user.getFirstName());
			preparedStmt.setString(7, user.getLastName());
			preparedStmt.setDate(8, Common.getDateFormatSQL(user.getBirthDate()));
			preparedStmt.setString(9, user.getPhone());
			preparedStmt.setString(10, user.getAddress());
			preparedStmt.setString(11, user.getLevelStudies());
			preparedStmt.setString(12, user.getSexo());
			preparedStmt.setBlob(13, user.getImage());

			result = preparedStmt.execute();
		} catch (Exception e) {
			System.err.println(e.getMessage());

		} finally {
			try {
				if (s != null)
					s.close();
				if (rs != null)
					rs.close();
				if (conn != null)
					conn.close();

			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return result;
	}
}
