package test;

import static org.junit.Assert.*;

import java.sql.SQLException;
import java.util.Date;

import org.junit.Test;

import model.DBManager;
import model.User;
import view.Login;
import view.ViewManager;

public class ValidateLoginTest {

	@Test
	public void testLogin() throws SQLException {
		DBManager db = new DBManager();
		User userLoged = db.validaLogin("moha", "moha");
		assertTrue(userLoged != null);
		
		String nombreEsperado = "Mohamed";
		assertEquals(nombreEsperado, userLoged.getName());
	}
}
