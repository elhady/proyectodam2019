package test;

import static org.junit.Assert.assertTrue;


import org.junit.Test;
import view.AddUser;
import view.Fichar;
import view.Login;
import view.ManageRecords;
import view.ViewBase;
import view.ViewHeader;
import view.ViewManager;

public class InstanceOfTest {
	ViewManager vm = new ViewManager();
	 @Test
	    public void testInstanceOfAddUser() {
			AddUser addUser = new AddUser(vm);
			assertTrue(addUser instanceof ViewHeader);
	    }
	 
	 @Test
	    public void testInstanceOfFichar() {
			Fichar fichar = new Fichar(vm);
			assertTrue(fichar instanceof ViewHeader);
	    }
	 
	 @Test
	    public void testInstanceOfManageRecords() {
			ManageRecords mr = new ManageRecords(vm);
			assertTrue(mr instanceof ViewHeader);
	    }
	 
	 @Test
	    public void testInstanceOfLogin() {
		 Login login = new Login(vm);
			assertTrue(login instanceof ViewBase);
	    }
	 
	 
	 

}
