package controller;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import general.Common;
import general.Const;
import model.DBManager;
import model.Device;
import model.FileControl;
import model.User;
import view.ViewManager;

/**
 * Clase que negocia los datos entre la interfice y la BBDD.
 */
public class Controller implements Observer {

	ViewManager viewManager;
	static DBManager dbManager;

	public Controller() {
		try {
			dbManager = new DBManager();
			viewManager = new ViewManager();
			viewManager.addObserver(this);
			viewManager.init();

		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}

	}

	public static User validaLogin(String user, String password) {
		User userLoged = (User) dbManager.validaLogin(user, password);
		return userLoged;
	}

	@Override
	public void update(Observable o, Object arg) {
		Map data = (Map) arg;
		String event = (String) data.get("event");

		switch (event) {
		case Const.EV_LOGIN:
			User result = dbManager.validaLogin((String) data.get("user"), (String) data.get("pass"));
			viewManager.loginResult(result);
			break;
		case Const.EV_FICHAR:
			User user = (User) data.get("user");
			Date dateLogin = (Date) data.get("DateLogin");
			char typeRegister = dbManager.getTypeRegister(user);
			Device device = new Device(new Date(), Common.getCurrentMAC(), Common.getCurrentIP());

			viewManager.ficharResult(typeRegister, dbManager.fichar(user, device, dateLogin, typeRegister));
			break;
		case Const.EV_ADDUSER:
			user = (User) data.get("user");
			if (!isExistUser(user)) {
				viewManager.addUserResult(dbManager.insertUser(user));
			}
			break;
		case Const.EV_REGISTERS:
			user = (User) data.get("user");
			java.util.Date dateIn = Common.isEmptyDate(data.get("dateIn").toString()) ? null
					: Common.getDateFormat((String) data.get("dateIn"), Common.dateFormt);
			java.util.Date dateFi = Common.isEmptyDate(data.get("dateFi").toString()) ? null
					: Common.getDateFormat((String) data.get("dateFi"), Common.dateFormt);
			ArrayList<FileControl> listFileControl = DBManager.getRegistrsFileControl(user, dateIn, dateFi);
			viewManager.insertRegistersToTable(listFileControl);
			break;
		default:
			break;
		}

	}

	private boolean isExistUser(User user) {
		boolean result = dbManager.isExistUser(user);
		// TODO
		return result;
	}

	/*
	 * public static void main(String[] args) throws SQLException { Connection conn
	 * = ConnectionManager.getConnection(); Statement s = conn.createStatement();
	 * 
	 * ResultSet rs = s.executeQuery("Select * from device");
	 * 
	 * while (rs.next()) { System.out.println(rs.getString("address_mac")); }
	 * 
	 * rs.close(); conn.close();
	 * 
	 * }
	 * 
	 */
}
