package general;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.JOptionPane;

import view.Login;
import view.ViewManager;

/**
 * Clase que se encarga de la Conexión a la base de datos.
 * @author elhady
 *
 */
public class ConnectionManager {
	private static String url = "jdbc:mariadb://localhost:3306/dbcontroltime?";
	private static String username = "moha";
	private static String password = "moha";
	private static Connection dm;
	/**
	 * Método que se encarga de establecer la conexión con la base de datos.
	 * @return DriverManager
	 */
	public static Connection getConnection() {
		try {
			 dm = DriverManager.getConnection(url, username, password);

		} catch (SQLException e) {
			System.err.println("Error d'establiment de connexió: " + e.getMessage());
		}

		return dm;
	}
}

