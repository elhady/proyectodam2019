package general;

/**
 * Clase de Constantes
 * 
 * @author elhady
 *
 */
public final class Const {

	// Tipo de fichaje
	public static final char TYPE_ENTRY = 'E';
	public static final char TYPE_EXIT = 'S';
	public static final String TYPE_USER_ADMIN = "A";
	public static final String TYPE_USER_USER = "U";
	public static final String TYPE_SEXO_HOMBRE = "H";
	public static final String TYPE_SEXO_MUJER = "M";
	public static final int PANTALLA_X = 925;
	public static final int PANTALLA_Y = 570;
	public static final int INSERT_OK = 1;
	// Eventos observer
	public static final String EV_FICHAR = "ficharClick";
	public static final String EV_LOGIN = "loginClick";
	public static final String EV_ADDUSER = "addUserClick";
	public static final String EV_REGISTERS = "getRegistersClick";

}
