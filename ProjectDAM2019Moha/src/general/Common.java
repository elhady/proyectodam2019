package general;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URL;
import java.net.UnknownHostException;
import java.sql.Blob;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

import javax.imageio.ImageIO;
import javax.sql.rowset.serial.SerialBlob;
import javax.sql.rowset.serial.SerialException;
import javax.swing.JFormattedTextField;
import javax.swing.text.MaskFormatter;

import model.User;

/**
 * Clase con métodos genéricos.
 * 
 * @author elhady
 *
 */
public class Common {

	public static DateFormat dateTimeFormat = new SimpleDateFormat("HH:mm:ss - dd/MM/yyyy");
	public static DateFormat dateTimeFormatSQL = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	public static SimpleDateFormat TimeFormat = new SimpleDateFormat("HH:mm:ss");
	public static final SimpleDateFormat dateFormt = new SimpleDateFormat("dd/MM/yyyy");

	/**
	 * Método que devuelve la fecha entrada en los parámetros con el formato
	 * indicado.
	 * 
	 * @param d Fecha
	 * @param f Formato
	 * @return String
	 */
	public static String getDateFormatST(java.util.Date d, SimpleDateFormat f) {
		String result = f.format(d);
		return result;
	}
	


	
	/**
	 * Método que formtea la fecha entrada de String a Date dd-MMM-yyyy
	 * @param date
	 * @return
	 */
	public static Date getDateFormat(java.util.Date date, SimpleDateFormat format) {
		SimpleDateFormat formatter = format;
		Date dateFormat = null;
		try {
			dateFormat = (Date) formatter.parse(String.valueOf(date));

		} catch (ParseException e) {
			System.err.println(e.getMessage());
		}

		return dateFormat;
	}
	


	
	
	public static java.util.Date getDateFormat(String date, SimpleDateFormat format) {
		SimpleDateFormat formatter = format;
		java.util.Date dateFormat = null;
		try {
			dateFormat = (java.util.Date) formatter.parse(date);

		} catch (ParseException e) {
			System.err.println(e.getMessage());
		}

		return dateFormat;
	}
	
	
	

	/**
	 * Método comprueba si una cadena es un vació o null.
	 * 
	 * @param value Cadena que se desea comprobar
	 * @return boolean
	 */
	public static boolean isEmpty(String value) {
		return (value == null || value == "" || value.equals("")) ? true : false;
	}
	
	public static boolean isEmptyDate(String value) {
			for(int i=0; i<value.length(); i++) {
				if(Character.isDigit(value.charAt(i))) {
					return false;
				}
			}
		return true;//(value == null || value == "" || value.equals("")) ? true : false;
	}

	/**
	 * Método comprueba si un objeto es null.
	 * 
	 * @param Obj Objeto que se desea comprobar
	 * @return boolean
	 */
	public static boolean isEmpty(Object obj) {
		return (obj == null) ? true : false;
	}

	/**
	 * Método que concatena el nombre y apellidos de usuario.
	 * 
	 * @param user objeto User
	 * @return String
	 */
	public static String getCompletNameUser(User user) {
		String result = user.getName() + " " + user.getFirstName() + " " + user.getLastName();
		return result;
	}

	/*
	 * I am not sure if you can grab that IP from code that runs on the local
	 * machine. You can however build code that runs on a website, say in JSP, and
	 * then use something that returns the IP of where the request came from:
	 * request.getRemoteAddr() Or simply use already-existing services that do this,
	 * then parse the answer from the service to find out the IP.
	 */
	/**
	 * Método que devuelce la IP de la maquina desde la cual se ha conectado el
	 * usuario.
	 * 
	 * @return String
	 */
	public static String getCurrentIP() {
		String result = null;
		boolean tengoIp = false;
		URL whatismyip = null;

		try {
			whatismyip = new URL("http://checkip.amazonaws.com");
			tengoIp = Common.isEmpty(whatismyip.openStream());
		} catch (Exception e) {
		}

		try {

			if (tengoIp) {
				InputStreamReader in = new InputStreamReader(whatismyip.openStream());
				BufferedReader br = new BufferedReader(in);
				result = br.readLine();
			}
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return result = (Common.isEmpty(result) ? "NO INTERNET" : result);
	}

	/**
	 * Método que devuelce la MAC de la maquina desde la cual se ha conectado el
	 * usuario.
	 * 
	 * @return String
	 */
	public static String getCurrentMAC() {
		String result = null;

		try {

			NetworkInterface netInf = NetworkInterface.getNetworkInterfaces().nextElement();
			byte[] mac = netInf.getHardwareAddress();

			StringBuilder sb = new StringBuilder();
			if (!Common.isEmpty(mac)) {
				for (int i = 0; i < mac.length; i++) {
					sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));
				}
				result = sb.toString();
			}
		} catch (SocketException e) {
			e.printStackTrace();
		}

		return result = (Common.isEmpty(result) ? "NO INTERNET" : result);
	}

	/**
	 * Método que devuelve la fecha que se le indica por parametros en formato DATE
	 * SQL
	 * 
	 * @param date Fecha Java
	 * @return sql.Date
	 */
	public static Date getDateFormatSQL(java.util.Date date) {
		GregorianCalendar gr = new GregorianCalendar();
		gr.setTime(date);
		java.sql.Date sql = new java.sql.Date(gr.getTimeInMillis());

		return sql;
	}

	public static Time getTime(java.util.Date date) {
		GregorianCalendar gr = new GregorianCalendar();
		gr.setTime(date);

		return new Time(gr.getTime().getTime());
	}

	public static String separatorsToSystem(String res) {
		if (res == null)
			return null;
		if (File.separatorChar == '\\') {
			// From Windows to Linux/Mac
			return res.replace('/', File.separatorChar);
		} else {
			// From Linux/Mac to Windows
			return res.replace('\\', File.separatorChar);
		}
	}

	/**
	 * Método que crea una imagen en formato Blob desde una Imagen
	 * 
	 * @param imagen    imagen que se desea parsear
	 * @param extension extensión de la imagen
	 * @return Blob
	 */
	public static Blob convertirImagenABlob(Image imagen, String extension) {
		Blob imagenBlob = null;
		BufferedImage bi = new BufferedImage(imagen.getWidth(null), imagen.getHeight(null),
				BufferedImage.TYPE_INT_ARGB);
		Graphics bg = bi.getGraphics();
		bg.drawImage(imagen, 0, 0, null);
		bg.dispose();

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			ImageIO.write(bi, extension, baos);
			baos.flush();
			baos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		byte[] imagenByte = baos.toByteArray();

		try {
			imagenBlob = new SerialBlob(imagenByte);
		} catch (SerialException se) {
			se.printStackTrace();
		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}
		return imagenBlob;
	}

	public static JFormattedTextField getJFormattedTextField() {
		JFormattedTextField result = null;
		try {
			MaskFormatter maskDate = new MaskFormatter("##/##/####");
			maskDate.setPlaceholderCharacter(' ');
			result = new JFormattedTextField(maskDate);

		} catch (ParseException e1) {
			e1.printStackTrace();
			System.err.println(e1.getMessage());
		}
		return result;
	}
	
}
